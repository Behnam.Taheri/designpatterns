﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public class DashWriter : Writer
    {
        public override void Write()
        {
            base.Write();
            Console.WriteLine("---------------------------------------");
        }
    }
}

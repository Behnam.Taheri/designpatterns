﻿using System;

namespace Decorator
{
    public class StarWriterDecorator : IWriter
    {
        private readonly IWriter writer;

        public StarWriterDecorator(IWriter writer)
        {
            this.writer = writer;
        }

        public void Write()
        {
            Console.WriteLine("**************************************");
            writer.Write();
            Console.WriteLine("**************************************");
        }
    }
}

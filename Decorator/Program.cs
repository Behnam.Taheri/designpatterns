﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            //Writer writer = new StarWriter();
            //writer.Write();


            IWriter writerDecorator = new StarWriterDecorator(new DashWriterDecorator(new StarWriterDecorator(new Writer())));
            writerDecorator.Write();
        }
    }
}

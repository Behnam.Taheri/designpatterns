﻿using System;

namespace Decorator
{
    public class DashWriterDecorator : IWriter
    {
        private readonly IWriter writer;

        public DashWriterDecorator(IWriter writer)
        {
            this.writer = writer;
        }

        public  void Write()
        {
            Console.WriteLine("---------------------------------------");
            writer.Write();
            Console.WriteLine("---------------------------------------");
        }
    }
}

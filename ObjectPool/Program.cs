﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ObjectPool
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectPool<MyClass> objPool = new ObjectPool<MyClass>();
            while (true)
            {
                MyClass obj = objPool.Get();
                obj.Id = Convert.ToInt32(Console.Read());

                objPool.Release(obj);

            }

        }
    }

    public class ObjectPool<T> where T : new()
    {
        private readonly ConcurrentBag<T> items = new ConcurrentBag<T>();
        //private readonly List<T> items = new List<T>();
        private int counter = 0;
        private int MAX = 10;
        public void Release(T item)
        {
            if (counter < MAX)
            {
                items.Add(item);
                counter++;
            }
        }
        public T Get()
        {
            T item;
            if (items.TryTake(out item))
            {
                counter--;
                return item;
            }
            else
            {
                T obj = new T();
                items.Add(obj);
                counter++;
                return obj;
            }
        }
    }
}

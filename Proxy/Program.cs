﻿using System;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            Proxy Proxy = new Proxy();
            Proxy.Request();
        }
    }
    public interface ISubject
    {
        void Request();
    }

    public class Proxy : ISubject
    {
        private RealSubject RealSubject;
        public void Request()
        {
            RealSubject = new RealSubject();
            RealSubject.Request();
        }
    }

    public class RealSubject : ISubject
    {
        public void Request()
        {
            Console.WriteLine("Its Call Successful;");
        }
    }

}

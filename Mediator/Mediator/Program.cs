﻿using System;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            SinkNode sinkNode = new SinkNode();


            HeatSensor heatSensor = new HeatSensor("hs1", sinkNode);
            HeatSensor heatSensor2 = new HeatSensor("hs2", sinkNode);

            //HeatSensor heatSensor = new HeatSensor("hs1");
            //HeatSensor heatSensor2 = new HeatSensor("hs2");

            //heatSensor.HandOver(heatSensor2);
            //heatSensor2.HandOver(heatSensor);

            //VibrationSensor vibrationSensor = new VibrationSensor("vs1");

            //vibrationSensor.HandOver(heatSensor);
            //vibrationSensor.HandOver(heatSensor2);

        }
    }
}

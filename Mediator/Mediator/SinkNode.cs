﻿using System.Collections.Generic;

namespace Mediator
{
    public class SinkNode
    {
        private List<Sensor> sensors = new List<Sensor>();

        public void HandOver(Sensor sensor)
        {
            sensors.Add(sensor);
        }

        public void Left(Sensor sensor)
        {
            sensors.Remove(sensor);
        }

        public void Send(Sensor sensor, string data)
        {
           
        }

    }

    public abstract class Sensor
    {
        private readonly SinkNode sinkNode;

        protected Sensor(SinkNode sinkNode)
        {
            this.sinkNode = sinkNode;
            sinkNode.HandOver(this);
        }

        protected string Model { get; set; }
    }

    public class HeatSensor : Sensor
    {

        public HeatSensor(string model, SinkNode sinkNode) : base(sinkNode)
        {
            Model = model;
        }
    }

    public class VibrationSensor : Sensor
    {
        public VibrationSensor(string model, SinkNode sinkNode) : base(sinkNode)
        {
            Model = model;
        }
    }

    public class SoundSensor : Sensor
    {
        public SoundSensor(string model, SinkNode sinkNode) : base(sinkNode)
        {
            Model = model;
        }
    }
}

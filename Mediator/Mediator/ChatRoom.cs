﻿using System.Collections.Generic;
using System.Linq;

namespace Mediator
{
    public class ChatRoom
    {
        private List<User> _users = new List<User>();
        public string Title { get; private set; }
        public ChatRoom(string title)
        {
            Title = title;
        }
        public void Join(User user)
        {
            _users.Add(user);
        }
        public void Send(User sender, string message)
        {
            var candidates = _users.Where(a => a != sender).ToList();
            foreach (var candidate in candidates)
            {
                candidate.Receive(message);
            }
        }
    }
}
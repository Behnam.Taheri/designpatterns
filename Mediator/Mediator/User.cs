﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediator
{
    public class User
    {
        private readonly ChatRoom _room;
        public string Name { get; set; }
        public User(string name, ChatRoom room)
        {
            _room = room;
            _room.Join(this);
            Name = name;
        }

        public void Send(string message)
        {
            _room.Send(this, message);
        }
        public virtual void Receive(string message)
        {
            //....
        }
    }
}

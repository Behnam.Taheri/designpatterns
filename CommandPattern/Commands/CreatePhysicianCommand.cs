﻿using CommandPattern.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandPattern.Commands
{
    public class CreatePhysicianCommand: ICommand
    {
        public string CellPhone { get; set; }
    }
}

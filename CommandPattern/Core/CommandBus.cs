﻿using CommandPattern.CommandHandlers;
using CommandPattern.Contracts;
using CommandPattern.Decorators;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandPattern.Core
{
    public class CommandBus : ICommandBus
    {
        private readonly IServiceProvider serviceProvider;

        public CommandBus(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public void Dispatch<T>(T command) where T : ICommand
        {
            var handles = serviceProvider.GetServices<ICommandHandler<T>>().ToList();
           
            handles.ForEach(x => new LoggingCommandHandlerDecorator<T>(x).Handle(command));
        }
    }
}

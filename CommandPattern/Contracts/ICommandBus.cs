﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandPattern.Contracts
{
    public interface ICommandBus
    {
        void Dispatch<T>(T command) where T : ICommand;
    }
}

﻿using CommandPattern.Commands;
using CommandPattern.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandPattern.CommandHandlers
{
    public class CreatePhysicianCommandHandler : ICommandHandler<CreatePhysicianCommand>
    {
        public void Handle(CreatePhysicianCommand command)
        {

        }
    }
}

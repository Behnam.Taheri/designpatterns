﻿using CommandPattern.Commands;
using CommandPattern.Contracts;
using System;

namespace CommandPattern.CommandHandlers
{
    public class ActivatePhysicianCommandHandler : ICommandHandler<CreatePhysicianCommand>
    {
        public void Handle(CreatePhysicianCommand command)
        {

        }
    }
}

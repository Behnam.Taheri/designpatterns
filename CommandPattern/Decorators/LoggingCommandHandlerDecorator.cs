﻿using CommandPattern.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CommandPattern.Decorators
{
    public class LoggingCommandHandlerDecorator<T> : ICommandHandler<T> where T : ICommand
    {
        private readonly ICommandHandler<T> commandHandler;

        public LoggingCommandHandlerDecorator(ICommandHandler<T> commandHandler)
        {
            this.commandHandler = commandHandler;
        }

        public void Handle(T command)
        {
            Debug.WriteLine("Start");
            commandHandler.Handle(command);
            Debug.WriteLine("End");
        }
    }
}

﻿using CommandPattern.Commands;
using CommandPattern.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandPattern.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhysicianController : ControllerBase
    {
        private readonly ICommandBus commandBus;

        public PhysicianController(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        [HttpPost]
        public IActionResult Create(CreatePhysicianCommand command)
        {
            commandBus.Dispatch(command);
            return Ok();
        }
    }
}

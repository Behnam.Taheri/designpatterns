﻿using System;
using System.Drawing;
using System.Text;

namespace Observer
{
    public class Observer1 : IObserver
    {

        private string _Color = "Green";

        public void ColorChanged(string newColor)
        {
            _Color = newColor;
            Console.WriteLine("1 " + _Color);
        }
    }

    public class Observer2 : IObserver
    {

        private string _Color = "Black";

        public void ColorChanged(string newColor)
        {
            _Color = newColor;
            Console.WriteLine("2 "+_Color);
        }
    }
}

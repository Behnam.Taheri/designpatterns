﻿using System.Drawing;

namespace Observer
{
    public interface IObserver
    {
        void ColorChanged(string newColor);
    }
}

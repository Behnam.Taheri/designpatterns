﻿using System.Collections.Generic;
using System.Drawing;

namespace Observer
{
    public class ColorSubject : ISubject
    {
        private string _Color = "Blue";

        public string Color
        {
            get { return _Color; }
            set
            {
                _Color = value;
                Notify();
            }
        }

        #region ISubject Members

        private List<IObserver> _observers = new List<IObserver>();

        public void Register(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Unregister(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify()
        {
            //foreach (var item in _observers)
            //{

            //}
            _observers.ForEach(o => o.ColorChanged(Color));
        }

        #endregion
    }
}

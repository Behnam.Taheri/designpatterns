﻿using System;
using System.Linq;
using System.Reflection;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            var types = Assembly.Load("Observer").GetTypes().Where(x=>typeof(IObserver).IsAssignableFrom(x) && !x.IsInterface);
            
            ColorSubject colorSubject = new ColorSubject();
            foreach (var item in types)
            {
                var instance = Activator.CreateInstance(item);
                colorSubject.Register(instance as IObserver);
            }

            Console.WriteLine("Write Color And Press Enter");
            string color = Console.ReadLine();

            colorSubject.Color = color;
            //colorSubject
        }
    }
}
